module Main where

import Data.List

-- Clasificacion 1 :-> save the world againg...
main :: IO ()
main = interact $
  unlines . zipWith output [1..] . map solve . parselines . tail . lines

output :: Int -> String -> String
output tc answer = "Case #" ++ show tc ++ ": " ++ answer

parselines :: [String] -> [[Int]]
parselines (_:line:ls) = map read (words line) : parselines ls
parselines _ = []

solve :: [Int] -> String
solve a = case munchi 0 $ orde a of
  Just n -> show n
  _ -> "OK"

munchi :: Int -> [(Int,Int)] -> Maybe Int
munchi n [] = Nothing
munchi n ((a,b): r) = if (a > b) then Just n else munchi (n+1) r

orde :: [Int] -> [(Int,Int)]
orde il =
  let m (a,b) c = if length a > length b then (a,(c:b)) else ((c:a), b)
      (l1, l2) = foldl m ([],[]) il
      sl1 = sort l1
      sl2 = sort l2
      bigSl = h sl1 sl2
  in  reverse $ snd $ foldl (\acc v -> (v, (fst acc, v) : snd acc)) (head bigSl,[]) (tail bigSl)

h :: [Int] -> [Int] -> [Int]
h [] a = a
h a [] = a
h (a:xs) (b:ys) = [a,b] ++ h xs ys