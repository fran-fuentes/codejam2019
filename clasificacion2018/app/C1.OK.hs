module Main where

import Data.Char

-- Clasificacion 1 :-> save the world againg...
main :: IO ()
main = interact $
  unlines . zipWith output [1..] . solveAll . tail . lines

output :: Int -> String -> String
output tc res = "Case #"  ++ show tc ++ ": " ++ res

solveAll :: [String] -> [String]
solveAll [] = []
solveAll (a:resto) = m : solveAll resto
  where (r,_,_,_) = solveOne a
        m = if r == -1  then  "IMPOSSIBLE" else (show r)

solveOne :: String -> (Int, Int, Int, String)
solveOne "" = (0, 0, 0, "")
solveOne ddata = 
  let (ms, rest) = maxShield 0 ddata
      (maxD, maxE, eses, program) = maxDamage rest
      res = 
        if maxD == 0 then 0
        else if eses > ms then  -1
             else resolveMovements maxD ms maxE program [] 0
  in 
    (res, maxD, maxE, program)

resolveMovements :: Int -> Int -> Int -> String -> String -> Int -> Int
resolveMovements _ _ _ "" _ movements = movements
resolveMovements damage shield energy ('C':program) [] movements = resolveMovements damage shield (div energy 2) program [] movements
resolveMovements damage shield energy ('S':program) bucket movements = resolveMovements damage shield energy program ('S':bucket) movements
resolveMovements damage shield energy ('C':program) (h:bucket) movements = 
  if damage <= shield then movements
  else resolveMovements (damage - (div energy 2))  shield energy ('C':h:program) bucket (movements+1)
resolveMovements damage shield energy (_:program) bucket movements = 
  resolveMovements damage shield energy program bucket movements 


maxShield:: Int -> String -> (Int, String)
maxShield v [] = (v, [])
maxShield v (' ':cad) = (v, cad)
maxShield v (a:cad) = maxShield (v*10 + (digitToInt a)) cad


-- Returns máxDamage, max shoot and the original program reversed
maxDamage :: String -> (Int, Int, Int, String)
maxDamage seed =
  dam 0 1 0 seed ""
    
-- damage, energy level 
dam :: Int -> Int -> Int -> String -> String -> (Int, Int, Int, String)
dam a b s "" c = (a, b, s, c)
dam a b s (op:rest) c
  | op == 'C' = dam a (b+b) s rest (op : c)
  | op == 'S' = dam (a+b) b (s+1)  rest (op : c)
  | otherwise = dam a b s rest (op : c)
