module Main where

import Data.Char
import Data.List
import GHC.IO.Handle
import GHC.IO.Handle.FD

main :: IO ()
main = do 
    nCases <- getLine 
    let (h, _) = getNumber 0 nCases
    let gen = [1..h]
    perr h
    sequence $ fmap resolve gen
    return ()


getNumber:: Int -> String -> (Int, String)
getNumber a [] = (a, [])
getNumber a (' ' : xs) = (a, xs)
getNumber a (b:xs) = 
  getNumber (a*10 + (digitToInt b)) xs


resolve:: Int -> IO (Maybe Bool)
resolve _ = do
  l <- getLine
  let (limit1, limit2) = getNumbers l
  l2 <- getLine
  let (nGuesses, _) = getNumber 0 l2
  perrL (limit1:limit2:nGuesses:[])
  guessOne limit1 limit2 nGuesses

guessOne :: Int -> Int -> Int -> IO (Maybe Bool)
guessOne _ _ 0 = do
  return Nothing
guessOne a b c = do
  let m = (a+b+1) `div` 2
  perr m
  print m
  print "\n"
  h2 <- getLine
  perr h2
  case h2 of
    "TOO_SMALL" -> guessOne m b (c-1)
    "TOO_BIG" -> guessOne a m (c-1)
    "CORRECT" -> return $ Just True
    "WRONG_ANSWER" -> return $ Just False

getNumbers :: String -> (Int, Int)
getNumbers str = 
  let (a, str') = getNumber 0 str in 
  let (b, s) = getNumber 0 str' in
  (a,b) 

perr :: (Show a) => a -> IO()
perr a  = do 
  perrL (a:[])

perrL :: (Show a) => [a] -> IO()
perrL [] = do
  hPutStr stderr $ "\n"
perrL (a:xs)= do
  hPutStr stderr $ show a
  hPutStr stderr $ " "
  perrL xs
